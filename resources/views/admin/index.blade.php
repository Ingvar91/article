<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Админ панель</title>

    <!-- bootstrap -->
    <link href="/static-admin/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- dashboard -->
    <link href="/static-admin/css/dashboard.css" rel="stylesheet">
    
    <!-- font-awesome -->
    <link href="/static-admin/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- custom -->
    <link href="/static-admin/css/custom.css" rel="stylesheet">
    
    <!-- PNotify -->
    <link href="/static-admin/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="/static-admin/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="/static-admin/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    @stack('css')
    
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{url('/admin/posts')}}">Админ панель</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="{{url('/')}}">Главная</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="{{url('/admin/posts')}}">Записи <span class="sr-only">(current)</span></a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            
            @yield('content')
            
        </div>
      </div>
    </div>
      
    <div id="loader">
        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
    </div>

    <!-- jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    
    <!-- bootstrap -->
    <script src="/static-admin/js/bootstrap.min.js"></script>
    
    <!-- PNotify -->
    <script src="/static-admin/vendors/pnotify/dist/pnotify.js"></script>
    <script src="/static-admin/vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="/static-admin/vendors/pnotify/dist/pnotify.nonblock.js"></script>
    
    @stack('scripts')
    
  </body>
</html>