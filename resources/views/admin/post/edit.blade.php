@extends('admin/index') 

@push('css')
<!-- trumbowyg -->
<link href="/static-admin/vendors/trumbowyg/dist/ui/trumbowyg.min.css" rel="stylesheet" >

<link href="/static-site/css/tags.css" rel="stylesheet">
@endpush 

@push('scripts')
<!-- trumbowyg -->
<script src="/static-admin/vendors/trumbowyg/dist/trumbowyg.min.js"></script>

<script src="/static-admin/js/post.js"></script>
@endpush 

@section('content')
<div id="form-edit-post">
    <form method="post">
        <input type="hidden" id="id" name="id" value="{{$post->id}}"/>
        <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="form-group">
                    <h2>Редактировать запись</h2>
                    <div class="clearfix"></div>
                </div>
                <div>
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <small class="text-muted">Создано: {{$post->created_at}}</small>
                            <br/>
                            <small class="text-muted">Последнее изменение: {{$post->updated_at}}</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 margin-bottom">
                            <label class="control-label" for="title">Заголовок</label>
                            <input id="title" name="title" class="form-control col-md-7 col-xs-12" type="text" autocomplete="off" placeholder="Заголовок" value="{{$post->title}}">
                            <div class="error-message hide">
                                <span class="control-label">Введите заголовок записи</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 margin-bottom">
                            <label class="control-label" for="text">Текст</label>
                            <div id="text" class="wysiwyg" placeholder="Текст">{!!$post->text!!}</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 margin-bottom">
                            <label class="control-label" for="annotation">Аннотация</label>
                            <textarea id="annotation" class="form-control" placeholder="Аннотация" autocomplete="off">{{$post->annotation}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    <button type="button" id="save-post" class="btn btn-info btn-lg width-1 margin-large-top">Обновить</button>
                </div>
                <hr/>
                <div class="form-group">
                    <h4>Добавить теги</h4>
                    
                    <div id="tags">
                        <div class="tag-case"></div>
                        <input type="text" autocomplete="off" class="form-control"/>
                        <div class="dropdown">
                            <ul class="dropdown-menu autocomplete"> 
                                
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </form>
</div>
@stop