@extends('admin/index') 

@push('css')
@endpush 

@push('scripts')
<!-- post -->
<script src="/static-admin/js/post.js"></script>
@endpush 

@section('content')

<h2 class="page-header">Записи</h2>
<a class="btn btn-info" href="{{url('/admin/post/add-form')}}">Добавить запись</a>

<table class="table table-striped">
    <thead>
        <tr>
            <th>Заголовок</th>
            <th>Дата</th>
            <th>Действие</th>
        </tr>
    </thead>
    <tbody>
        @if(isset($posts))
            @foreach($posts as $postVal)
            <tr>
                <td>{{$postVal->title}}</td>
                <td><div>Последнее изменение</div><div>{{$postVal->updated_at}}</div></td>
                <td>
                    <a class="btn btn-info" href="{{url('/admin/post/edit/'.$postVal->id)}}" title="Редактировать запись"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                    <button class="remove-post btn btn-danger" data-id="{{$postVal->id}}" title="Удалить запись"><i class="fa fa-times" aria-hidden="true"></i></button>
                </td>
            </tr>
            @endforeach
        @endif
    </tbody>
</table>

@if(isset($posts)) {{ $posts->links() }} @endif

@stop