@extends('site/index') 

@push('css')
@endpush 

@push('scripts')
@endpush 

@section('content')

<div class="row">

    <div class="col-sm-8 blog-main">

        <section>
            @if(isset($posts))
            @foreach($posts as $postVal)
            <article>
                <a class="h3" href="{{url('/post', ['id' => $postVal->id])}}">{{$postVal->title}}</a>
                <p>{{$postVal->annotation}}</p>
            </article>
            <hr/>
            @endforeach
            @endif
        </section>

        <nav>
            @if(isset($posts)) {{ $posts->links() }} @endif
        </nav>

    </div><!-- /.blog-main -->

    <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
        <div class="sidebar-module">
            <h4>Теги</h4>
            @if(isset($tags))
                <ol class="list-unstyled">
                    @foreach($tags as $tagVal)
                        <li><a href="{{url('/posts/tag', ['id' => $tagVal->id])}}">{{$tagVal->name}}</a></li>
                    @endforeach
                </ol>
            @endif
        </div>
    </div><!-- /.blog-sidebar -->

</div>

@stop