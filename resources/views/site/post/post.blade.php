@extends('site/index') 

@push('css')
@endpush 

@push('scripts')
@endpush 

@section('content')

<section class="case">
    @if(isset($post))
        <article>
            <h3>{{$post->title}}</h3>
            <p>{!!$post->text!!}</p>
        </article>
        <hr/>
    @endif
</section>

@stop