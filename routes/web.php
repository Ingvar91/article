<?php

//SITE
Route::get('/', ['uses' => 'Site\PostsController@index']);
Route::get('/posts', ['uses' => 'Site\PostsController@index']);
Route::get('/posts/tag/{id}', ['uses' => 'Site\PostsController@getPostsByTag'])->where('id', '[\d]+');
Route::get('/post/{id}', ['uses' => 'Site\PostsController@getArticle'])->where('id', '[\d]+');

//API
Route::group(['prefix' => 'api'], function(){
    Route::get('/getPosts', ['uses' => 'Api\PostsController@getPosts']);
    Route::get('/getArticle', ['uses' => 'Api\PostsController@getArticle']);
});

//ADMIN
Route::group(['prefix' => 'admin'], function(){
    //ЗАПИСИ
    Route::get('/posts', ['uses' => 'Admin\PostsController@index']);
    Route::get('/post/add-form', ['uses' => 'Admin\PostsController@addPostForm']); 
    Route::get('/post/edit/{id}', ['uses' => 'Admin\PostsController@editPostForm'])->where('id', '[\d]+'); 
    Route::post('/post/add', ['uses' => 'Admin\PostsController@addPost']);
    Route::put('/post/edit/{id}', ['uses' => 'Admin\PostsController@editPost'])->where('id', '[\d]+');
    Route::delete('/post/remove/{id}', ['uses' => 'Admin\PostsController@removePost'])->where('id', '[\d]+');  	
    
    //ТЕГИ
    Route::get('/tags/getTagsToPost', ['uses' => 'Admin\TagsController@getTagsToPost']);
    Route::get('/tags/findTagsByName', ['uses' => 'Admin\TagsController@findTagsByName']);
    Route::post('/tags/addTag', ['uses' => 'Admin\TagsController@addTag']);
});