<?php

namespace App\Http\Models\Api;

use Illuminate\Database\Eloquent\Model;
use DB;

class Posts extends Model{
    
    protected $table = 'posts';

    //получить записи
    public static function getPosts($request){
        return Posts::orderBy('id', 'desc')->take($request->count)->get();
    }
    
    //получить запись по id
    public static function getPostById($request){
        return Posts::where('id', '=', $request->id)->first();
    }
}