<?php

namespace App\Http\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class Posts extends Model{
    
    protected $table = 'posts';

    //получить записи
    public static function getPosts($request){
        return Posts::orderBy('id', 'desc')->paginate(10);
    }
    
    //получить запись по id
    public static function getPostById($request){
        return Posts::where('id', '=', $request->id)->first();
    }
    
    //добавить запись
    public static function addPost($request){
        return Posts::insertGetId($request->insert);
    }
    
    //удалить запись
    public static function removePost($request){
        return Posts::where('id', '=', $request->id)->delete();
    }
    
    //сохранить изменения запись
    public static function editPost($request){
        return Posts::where('id', '=', $request->id)->update($request->update);
    }
    
}
