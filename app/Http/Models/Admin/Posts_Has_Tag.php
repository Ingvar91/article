<?php

namespace App\Http\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Posts_Has_Tag extends Model{
    
    protected $table = 'posts_has_tag';
    
    //получить все теги для указанной записи
    public static function getTagsToPost($request){
        return Posts_Has_Tag::where('posts_has_tag.post_id', '=', $request->id)
                ->leftJoin('tags', 'posts_has_tag.tag_id', '=', 'tags.id')
                ->select(
                    'tags.id', 
                    'tags.name'
                )
                ->get();
    }
    
    public static function getHasTag($request){
        return Posts_Has_Tag::where('post_id', '=', $request->id)->get();
    }
    
    public static function addHasTag($insert){
        return Posts_Has_Tag::insert($insert);
    }
    
    public static function deleteHasTagByPostAndTag($post_id, $tag_id){
        return Posts_Has_Tag::where('post_id', '=', $post_id)->where('tag_id', '=', $tag_id)->delete();
    }
    
    public static function deleteHasTagByPost($post_id){
        return Posts_Has_Tag::where('post_id', '=', $post_id)->delete();
    }
}