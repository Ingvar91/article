<?php

namespace App\Http\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class Tags extends Model{
    
    protected $table = 'tags';

    //получить теги
    public static function getTags($request){
        return Tags::orderBy('id', 'desc')->take(30)->get();
    }
    
    //добавить тег
    public static function addTag($name){
        if($name){
            //проверяем, есть ли такой тег
            $tag = Tags::where('name', '=', $name)->first();
            if(!$tag){
                //если нет, вставляем
                return Tags::insertGetId(['name' => $name]);
            }
            else return $tag->id;
        }
    }
    
    //поиск тега по имени
    public static function findTagsByName($request){
        if($request->name){
            $select = Tags::addSelect('id', 'name');
            $arrayWord = explode(' ', trim($request->name));
            foreach ($arrayWord as $value) {
                $select->orWhere('name', 'like', '%'.$value.'%');
            }
            return $select->orderBy('id', 'desk')->take(10)->get();
        }
    }
}