<?php

namespace App\Http\Models\Site;

use Illuminate\Database\Eloquent\Model;
use DB;

class Posts_Has_Tag extends Model{
    
    protected $table = 'posts_has_tag';
    
    public static function getPostsByTag($request){
        return Posts_Has_Tag::where('posts_has_tag.tag_id', '=', $request->id)
                ->leftJoin('posts', 'posts_has_tag.post_id', '=', 'posts.id')
                ->select(
                    'posts.id', 
                    'posts.title', 
                    'posts.annotation'
                )
                ->paginate(10);
    }
}
