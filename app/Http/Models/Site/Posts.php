<?php

namespace App\Http\Models\Site;

use Illuminate\Database\Eloquent\Model;
use DB;

class Posts extends Model{
    
    protected $table = 'posts';

    //получить записи
    public static function getPosts($request){
        return Posts::orderBy('id', 'desc')->paginate(10);
    }
    
    //получить запись по id
    public static function getPostById($request){
        return Posts::where('id', '=', $request->id)->first();
    }
}
