<?php

namespace App\Http\Models\Site;

use Illuminate\Database\Eloquent\Model;
use DB;

class Tags extends Model{
    
    protected $table = 'tags';

    //получить теги
    public static function getTags($request){
        return Tags::orderBy('id', 'desc')->take(30)->get();
    }
}
