<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use Storage;

use App\Http\Controllers\Site\Lib\Validation;
use App\Http\Controllers\Site\Lib\Images;

class ProfileController extends Controller {

    use Validation;
    use Images;
    
    private static $tmpl = 'auth/profile';

    public function index(){
        $this->data['user'] = $this->getUser();
        Storage::makeDirectory('/upload/avatars');
        return view(self::$tmpl, $this->data);
    }
    
    public function update(Request $request){
        $this->validProfile($request);

        //создаем массив для обновления значений в БД
        $array = [];
        if($request['password']) $array['password'] = bcrypt($request['password']);
        if($request['nickname']) $array['nickname'] = $request['nickname'];
        if($request['name']) $array['name'] = $request['name'];
        if($request['surname']) $array['surname'] = $request['surname'];
        if($request['patronymic']) $array['patronymic'] = $request['patronymic'];
        if($request['phone']) $array['phone'] = $request['phone'];
        
        //если загружена аватарка
        $path = '';
        if($request->hasFile('avatar')){
            //загружаем аватарку на сервер
            $path = $this->uploadAvatarUser($request);
        }
        
        if($path){//если был сформирован новый путь для нового изображения
            $array['image'] = $path;//заносим в массив путь для нового изображения
            //удаляем старую аватарку
            Storage::delete(Auth::user()->image);
        }

        $updateUser =  DB::table('users')->where('id', '=', Auth::user()->id)->update($array);//обновляем инфу о юзере
        if($updateUser){//если что-нибудь было изменено
            return redirect('/profile')->with('message-success', 'Данные были успешно обновленны.');
        }
        else return redirect('/profile');
    }

}
