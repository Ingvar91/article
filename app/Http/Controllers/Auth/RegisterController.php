<?php

namespace App\Http\Controllers\Auth;

//use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Mail;
use DB;
use Storage;

use App\Http\Controllers\Site\Lib\Validation;
use App\Http\Controllers\Site\Lib\Images;

class RegisterController extends Controller{

    use Validation;
    use Images;

    protected $redirectTo = '/';
    private static $tmpl = 'auth/';
    
    public function index(){
        return view(self::$tmpl.'register', $this->data);
    }

    //создать юзера
    public function createUser(Request $request){
        $this->validReg($request);
        
        //создаем массив для добвления значений в БД
        $array = [];
        $array['email'] = $request['email'];
        $array['password'] = bcrypt($request['password']);
        $array['verification'] = str_random(32);
        if($request['nickname']) $array['nickname'] = $request['nickname'];
        if($request['name']) $array['name'] = $request['name'];
        if($request['surname']) $array['surname'] = $request['surname'];
        if($request['patronymic']) $array['patronymic'] = $request['patronymic'];
        if($request['phone']) $array['phone'] = $request['phone'];
        
        //если загружена аватарка
        $path = '';
        if($request->hasFile('avatar')){
            //загружаем аватарку на сервер
            $path = $this->uploadAvatarUser($request);
        }
        
        if($path) $array['image'] = $path;

        $createUser =  DB::table('users')->insert($array);//создаем юзера
        if($createUser){//если юзер создан успешно
            //отправляем ему письмо на почту для верификации
            Mail::send('emails/register', array(
                'nameSite' => config('app.name'), 
                'verification' => $array['verification'], 
                'email' => $array['email'],
                'password' => $array['password']
            ), function($message) use ($array){
                $message->from(config('mail.host'));//от кого
                $message->to($array['email']);//кому отправляем
                $message->subject('Завершение регистрации');//тема письма
            });
            
            //входим в аккаунт пользователя
            $auth = Auth::attempt(['email' => $request['email'], 'password' => $request['password']], true);
            //пишем юзеру сообщение о том что бы сходил на почту
            return redirect('/message')->with('message-success', 'Вам было отправленно письмо на почту для подтверждения регистрации.');
        }
        else {//если нет
            return redirect('/register')->with('message-error', 'Произошла ошибка при регистрации, попробуйте заного, или свяжитесь с нами через форму обратной связи.');
        }
    }
    
    //верификация пользователя
    public function verification(Request $request){
        $update = DB::table('users')->where('verification', '=', $request->verification)->update(['verification' => 1]);
        if($update){
            if(Auth::check()){
                Auth::user()->verification = 1;
            }
            return redirect('/message')->with('message-success', 'Регистрация была завершена успешна.');
        }
        else return redirect('/message')->with('message-error', 'Ошибка верификации пользователя, вероятно пользователь уже подтвердил свой E-mail.');
    }
    
    //форма отправки письма для верификации пользователя
    public function confirmMailForm(Request $request){
        return view(self::$tmpl.'confirmMailForm', $this->data);
    }
    
    //отправить письмо юзеру для подтверждения регистрации
    public function sendMailConfirm(Request $request){
        $this->validCaptcha($request);
            
        //если email был указан не аутентифицированным пользователем
        if($request->email) $user = DB::table('users')->where('email', '=', $request->email)->first();
        else $user = $this->getUser();
        
        Mail::send('emails/register', array(
            'nameSite' => config('app.name'), 
            'verification' => $user['verification'], 
            'email' => $user['email']
            ), function($message) use ($user){
                $message->from(config('mail.host'));//от кого
                $message->to($user['email']);//кому отправляем
                $message->subject('Завершение регистрации');
        });
        return redirect('/message')->with('message-success', 'Письмо отправленно, проверьте ваш почту.');
    }
}
