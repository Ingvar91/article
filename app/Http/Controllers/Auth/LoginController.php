<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Validator;
use Illuminate\Http\Request;

class LoginController extends Controller {

    protected $redirectTo = '/';
    private static $tmpl = 'auth/login';

    // $2y$10$ZBvsE9BE2oTYeAnhtko/YO0h2l3pBnNkEPdDw/dbSqPBwp4PJFuIW (987025516)

    public function __construct() {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function index() {
        return view(self::$tmpl, $this->data);
    }

    public function logout() {
        Auth::logout();
        return back();
    }

    protected function validLogin(array $data, $messages = '') {
        return Validator::make($data, [
                'email' => 'required|email|max:255|exists:users,email',
                'password' => 'required',
            ], $messages);
    }

    //авторизация юзера
    public function login(Request $request) {
        $messages = [
            'email.required' => 'Поле E-mail не должно быть пустым.',
            'email.exists' => 'Такого пользователя не существует.',
            'password.required' => 'Поле пароля не должно быть пустым.'
        ];

        $validator = $this->validLogin($request->all(), $messages);
        if ($validator->fails()) {
            $this->throwValidationException($request, $validator);
        }

        //запоминаем юзера или нет
        if ($request->remember) $remember = true;
        else $remember = false;

        $auth = Auth::attempt(['email' => $request->email, 'password' => $request->password], $remember);

        if ($auth == true) {
            return back();
        } else {//если аторизация не прошла
            $validator->errors()->add('password', 'Ошибка, возможно вы ввели неверный пароль.');
            $this->throwValidationException($request, $validator);
        }
    }

}
