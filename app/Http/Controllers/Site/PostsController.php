<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Models\Site\Posts;
use App\Http\Models\Site\Tags;
use App\Http\Models\Site\Posts_Has_Tag;
use Illuminate\Http\Request;

class PostsController extends Controller{
    
    private static $tmpl = 'site/post/';
    
    //получить записи
    public function index(Request $request){
        $posts = Posts::getPosts($request);
        $tags = Tags::getTags($request);
        return view(self::$tmpl.'posts', ['posts' => $posts, 'tags' => $tags]);
    }
    
    //получить запись
    public function getArticle(Request $request){
        $post = Posts::getPostById($request);
        if(!$post) abort(404);
        return view(self::$tmpl.'post', ['post' => $post]);
    }
    
    public function getPostsByTag(Request $request){
        $posts = Posts_Has_Tag::getPostsByTag($request);
        $tags = Tags::getTags($request);
        return view(self::$tmpl.'posts', ['posts' => $posts, 'tags' => $tags]);
    }
}