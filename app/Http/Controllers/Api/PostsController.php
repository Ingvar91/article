<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Models\Api\Posts;
use Illuminate\Http\Request;

class PostsController extends Controller{
    
    //получить записи
    public function getPosts(Request $request){
        if(!$request->count) return response()->json(['error' => ['message' => 'invalid_count_value']]);
        $posts = Posts::getPosts($request);
        return response()->json($posts);
    }
    
    //получить запись
    public function getArticle(Request $request){
        if(!$request->id) return response()->json(['error' => ['message' => 'invalid_id_value']]);
        $post = Posts::getPostById($request);
        return response()->json($post);
    }
}