<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Models\Admin\Posts;
use App\Http\Models\Admin\Posts_Has_Tag;
use Illuminate\Http\Request;

class PostsController extends Controller {
    
    private static $tmpl = 'admin/post/';
    
    //получить записи
    public function index(Request $request){
        $posts = Posts::getPosts($request);
        return view(self::$tmpl.'posts', ['posts' => $posts]);
    }
    
    //удалить запись
    public function removePost(Request $request){
        return response()->json(['status' => Posts::removePost($request)]);
    }
    
    //форма добавления новой записи
    public function addPostForm(){
       return view(self::$tmpl.'add');
    }
    
    //добавить запись
    public function addPost(Request $request){
        $id = Posts::addPost($request);
        if($id){
            $this->addTag($request->tags, $id);
            return response()->json(['status' => 1]);
        }
        else return response()->json(['status' => 0]);
    }
    
    //форма редактирования записи
    public function editPostForm(Request $request){
        $post = Posts::getPostById($request);
        return view(self::$tmpl.'edit', ['post' => $post]);
    }
    
    //сохранить изменения записи
    public function editPost(Request $request){
        $oldTags = $this->getIdsTag(Posts_Has_Tag::getHasTag($request));//получаем id старых тегов
        $newTags = $request->tags;

        if(!count($newTags)){//если массив пуст, тогда удаляем все теги которые принадлежат этой записи
            Posts_Has_Tag::deleteHasTagByPost($request->id);
        }
        else{
            //вычисляем расхождение массивов
            $addTag = array_values(array_diff($newTags, $oldTags));//id тегов для добавления
            $removeTag = array_values(array_diff($oldTags, $newTags));//id тегов для удаления

            //добавляем новые теги для поста
            $this->addTag($addTag, $request->id);

            //удаляем оставшиеся теги при вычислении расхождения массивов
            if(count($removeTag) > 0){
                foreach ($removeTag as $value) {
                    Posts_Has_Tag::deleteHasTagByPostAndTag($request->id, $value);
                }
            }
        }
        return response()->json(['status' => Posts::editPost($request)]);
    }
    
    private function addTag($tags, $post_id){
        if(count($tags) > 0){
            $insert = [];
            foreach ($tags as $value) {
                $insert[] = ['post_id' => $post_id, 'tag_id' => $value];
            }
            Posts_Has_Tag::addHasTag($insert);
        }
    }
    
    private function getIdsTag($array){
        $oldTags = [];
        foreach ($array as $value){
            $oldTags[] = $value['tag_id'];
        }
        return $oldTags;
    }
}
