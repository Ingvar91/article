<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Models\Admin\Tags;
use App\Http\Models\Admin\Posts_Has_Tag;
use Illuminate\Http\Request;

class TagsController extends Controller{
    
    //получить все теги для указанной записи
    public function getTagsToPost(Request $request){
        return response()->json(Posts_Has_Tag::getTagsToPost($request));
    }
    
    //поиск тега по имени
    public function findTagsByName(Request $request){
        return response()->json(Tags::findTagsByName($request));
    }
    
    //добавить тег
    public function addTag(Request $request){
        $name = $request->tagName;
        if(is_string($name)){
            $id = Tags::addTag($name);
            if($id) return response()->json(['id' => $id, 'name' => $name]);
        }
    }
    
}
