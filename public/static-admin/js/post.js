(function ($) {

    //стлизация уведомлений
    PNotify.prototype.options.styling = "fontawesome";

    //инициализируем визивик
    if ($('.wysiwyg').length) {
        $('.wysiwyg').trumbowyg();
    }

    //добавить запись
    $('#add-post').on('click', function () {
        var title = $('#title').val(),
            text = $('#text').html(),
            annotation = $('#annotation').val(),
            tagsIds = getTagsId();

        if (!title) {//если заголовок отсутствует, выводим сообщение об этом
            $('#title').addClass('required').parents('.form-group').addClass('has-error').find('.error-message').removeClass('hide')
            return;
        }

        var insert = {
            title: title,
            text: text,
            annotation: annotation
        }

        $.ajax({
            beforeSend: function () {
                $('#loader').addClass('active');
            },
            url: '/admin/post/add',
            type: 'POST',
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                insert: insert,
                tags: tagsIds
            },
            complete: function () {
                $('#loader').removeClass('active');
            },
            success: function (data) {
                console.log(data)
                if (data.status) {
                    clearFields();//очищаем поля
                    new PNotify({
                        title: "Готово",
                        type: "success",
                        text: "Запись успешно добавлена"
                    });
                } else {
                    new PNotify({
                        title: "Ошибка",
                        type: "error",
                        text: "Произошла ошибка при добавлении записи"
                    });
                }
            }
        });
    });

    //сохранить запись
    $('#save-post').on('click', function () {
        var title = $('#title').val(),
            text = $('#text').html(),
            id = $('#id').val(),
            annotation = $('#annotation').val(),
            tagsIds = getTagsId()
            
        if (!title) {//если заголовок отсутствует, выводим сообщение об этом
            $('#title').addClass('required').parents('.form-group').addClass('has-error').find('.error-message').removeClass('hide')
            return;
        }

        var update = {
            title: title,
            text: text,
            annotation: annotation
        }

        $.ajax({
            beforeSend: function () {
                $('#loader').addClass('active');
            },
            url: '/admin/post/edit/' + id,
            type: 'PUT',
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                update: update,
                tags: tagsIds
            },
            complete: function () {
                $('#loader').removeClass('active');
            },
            success: function (data) {
                console.log(data)
                if (data.status) {
                    new PNotify({
                        title: "Готово",
                        type: "success",
                        text: "Запись успешно обновлена"
                    });
                } else {
                    new PNotify({
                        title: "Ошибка",
                        type: "error",
                        text: "Произошла ошибка при обновлении записи"
                    });
                }
            }
        });
    });

    //удалить запись
    $('.remove-post').on('click', function () {
        var id = $(this).data('id')
        if (confirm("Вы хотите удалить запись?")) {
            $.ajax({
                beforeSend: function () {
                    $('#loader').addClass('active');
                },
                url: '/admin/post/remove/' + id,
                type: 'DELETE',
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                complete: function () {
                    $('#loader').removeClass('active');
                },
                success: function (data) {
                    if (data.status) {
                        location.reload();//если ответ от сервера положительный, обновляем страницу
                    } else {
                        new PNotify({
                            title: "Ошибка",
                            type: "error",
                            text: "Произошла ошибка при удалении записи"
                        });
                    }
                }
            });
        }
    });

    //очистить поля формы
    function clearFields() {
        $('#title').val('')
        $('#annotation').val('')
        $('#text').text('')
    }

    $('#form-add-post, #form-edit-post').on('keyup', '.required', function () {
        $(this).removeClass('required').parents('.form-group').removeClass('has-error').find('.error-message').addClass('hide')
    });

    //вставка тегов в поле на странице редактирования записи
    if ($('#id').val()) {
        $.ajax({
            url: '/admin/tags/getTagsToPost',
            type: 'GET',
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                id: $('#id').val()
            },
            success: function (data) {
                addTags(data);
            }
        });
    }

    if (!String.prototype.trim) {
        (function () {
            // Вырезаем BOM и неразрывный пробел
            String.prototype.trim = function () {
                return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
            };
        })();
    }
    
    var tags = $('#tags');
    
    //добавление тега в БД если он отсутствует при нажатии на клавишу enter
    $('#tags input').on('keyup', function (e) {
        if (e.keyCode === 13) {
            addTag($(this).val().trim());
        }
    });
    
    //удалить тег из набора
    $('#tags').on('click', '.close', function () {
        $(this).parent().remove()
    });
    
    //добавить теги из массива
    function addTags(tagsData){
        $.each(tagsData, function (key, val) {
            appendTagTmpl(val.id, val.name);
        });
    }
    
    //добавить новый тег в БД
    function addTag(tagName){
        $.ajax({
            url: '/admin/tags/addTag',
            type: 'POST',
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                tagName: tagName
            },
            success: function(data){
                if (data.name) {
                    if(data.id){
                        appendTagTmpl(data.id, data.name);
                    }
                    $('#tags input').val('');
                }
            }
        });
    }
    
    //автоподстановка тегов от 2-х введеных символов
    $('#tags input').on('keyup', function(){
        var name = $('#tags input').val().trim()
        if (name.length >= 2) {
            $.ajax({
                url: '/admin/tags/findTagsByName',
                type: 'GET',
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    name: name
                },
                success: function (data) {
                    var tmplTags = '';
                    $.each(data, function (key, val) {
                        tmplTags += ' <li data-id="'+val.id+'" data-name="'+val.name+'"><a href="#">'+val.name+'</a></li> ';
                    });
                    $('#tags .autocomplete').html(tmplTags)
                    $('#tags .autocomplete').show()
                }
            });
        }
        else hideAutocomplite();
    });
    
    //добавляем из выпадающего списка выбранный тег
    $('#tags .autocomplete').on('click', 'li', function(){
        var $this = $(this);
        appendTagTmpl($this.attr('data-id'), $this.attr('data-name'));
        hideAutocomplite();
        $('#tags input').val('');
    });
    
    function hideAutocomplite(){
        $('#tags .autocomplete').html('')
        $('#tags .autocomplete').hide()
    }
    
    function appendTagTmpl(id, name){
        if(!$('#tags > .tag-case').find('[data-id="'+id+'"]').length){
            $('#tags > .tag-case').append(' <span data-id="'+id+'" data-name="'+id+'">'+name+' <i class="close fa fa-times" aria-hidden="true"></i></span> ');
        }
    }
    
    //получить id вставленных тегов
    function getTagsId(){
        var ids = [];
        $.each($('#tags > .tag-case >'), function (key, val) {
            ids[ids.length] = $(val).attr('data-id')
        });
        return ids;
    }
    

})(jQuery);